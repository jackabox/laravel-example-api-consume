<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Http;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Example GET
Route::get('/', function () {
    $response = Http::get('https://jsonplaceholder.typicode.com/posts');

    if ($response->status() != 200) {
        return 'Error occured';
    }

    dd($response->json());
});


// Example POST
Route::post('/', function () {
    $response = Http::post('https://jsonplaceholder.typicode.com/posts', [
        'title' => 'Steve',
        'body' => 'lorem ipsum',
        'userId' => 1
    ]);

    if ($response->status() != 200) {
        return 'Error occured';
    }

    dd($response->json());
});
